﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingBasket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingBasket.Tests
{
    [TestClass()]
    public class BuyTwoXGetYDiscountTests
    {
        [TestMethod()]
        public void BuyTwoXGetYDiscountTest()
        {
            IDiscount discount = new BuyTwoXGetYDiscount("Apple","Banana", 1);
            Assert.IsNotNull(discount);
        }

        [TestMethod()]
        public void CalculateDiscountTest()
        {
            IDiscount discount = new BuyTwoXGetYDiscount("Apple", "Banana", 1);
            Dictionary<string, BasketItem> items = new Dictionary<string, BasketItem>();
            BasketItem item = new BasketItem(new Product(0, "Apple", 3));

            item.SetAmount(4);
            items.Add(item.Item.Name, item);

            item.Item = new Product(0, "Banana", 5);
            item.SetAmount(4);
            items.Add(item.Item.Name, item);

            Assert.AreEqual(discount.CalculateDiscount(items), 10);
        }

        [TestMethod()]
        public void IsApplicableTest()
        {
            IDiscount discount = new BuyTwoXGetYDiscount("Apple", "Banana", 1);
            Dictionary<string, BasketItem> items = new Dictionary<string, BasketItem>();
            BasketItem item = new BasketItem(new Product(0, "Apple", 3));

            item.SetAmount(4);
            items.Add(item.Item.Name, item);

            item.Item = new Product(0, "Banana", 5);
            item.SetAmount(4);
            items.Add(item.Item.Name, item);

            Assert.AreEqual(discount.IsApplicable(items),true);
        }

        [TestMethod()]
        public void ToStringTest()
        {
            IDiscount discount = new BuyTwoXGetYDiscount("Apple", "Banana", 1);

            Assert.IsTrue(discount.ToString() is string);
        }
    }
}