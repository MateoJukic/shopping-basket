﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingBasket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingBasket.Tests
{
    [TestClass()]
    public class BasketItemTests
    {
        [TestMethod()]
        public void BasketItemTest()
        {
            Product product = new Product(0, "Apple", 2);
            BasketItem item = new BasketItem(product);
            Assert.IsNotNull(item);
        }

        [TestMethod()]
        public void GetAmount_Equals_One_After_Initialization()
        {
            Product product = new Product(0, "Apple", 2);
            BasketItem item = new BasketItem(product);
            Assert.AreEqual(item.GetAmount(),1);
        }

        [TestMethod()]
        public void Amount_Equals_30()
        {
            Product product = new Product(0, "Apple", 2);
            BasketItem item = new BasketItem(product);
            item.SetAmount(30);
            Assert.AreEqual(item.GetAmount(), 30);
        }

        [TestMethod()]
        public void Amount_Equals_5()
        {
            Product product = new Product(0, "Apple", 2);
            BasketItem item = new BasketItem(product);
            item.Add();
            item.Add();
            item.Add();
            item.Add();
            Assert.AreEqual(item.GetAmount(), 5);
        }

        [TestMethod()]
        public void ToStringTest()
        {
            Product product = new Product(0, "Product", 0);
            BasketItem item = new BasketItem(product);
            Assert.IsTrue(item.ToString() is string);
        }
    }
}