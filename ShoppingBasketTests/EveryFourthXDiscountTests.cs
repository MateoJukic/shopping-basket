﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingBasket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingBasket.Tests
{
    [TestClass()]
    public class EveryFourthXDiscountTests
    {
        [TestMethod()]
        public void EveryFourthXDiscountTest()
        {
            IDiscount discount = new EveryFourthXDiscount("Apple", 1);
            Assert.IsNotNull(discount);
        }

        [TestMethod()]
        public void Discount_Equals_4()
        {
            IDiscount discount = new EveryFourthXDiscount("Apple", 1);
            Dictionary<string, BasketItem> items = new Dictionary<string, BasketItem>();
            BasketItem item = new BasketItem(new Product(0, "Apple", 3));

            item.SetAmount(4);
            items.Add("Apple", item);

            Assert.AreEqual(discount.CalculateDiscount(items), 3);
        }

        [TestMethod()]
        public void IsApplicableTest()
        {
            IDiscount discount = new EveryFourthXDiscount("Apple", 1);
            Dictionary<string, BasketItem> items = new Dictionary<string, BasketItem>();
            BasketItem item = new BasketItem(new Product(0, "Apple", 3));

            item.SetAmount(4);
            items.Add("Apple", item);

            Assert.AreEqual(discount.IsApplicable(items), true);
        }

        [TestMethod()]
        public void ToStringTest()
        {
            IDiscount discount = new EveryFourthXDiscount("Apple", 1);
            
            Assert.IsTrue(discount.ToString() is string);
        }
    }
}