﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingBasket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingBasket.Tests
{
    [TestClass()]
    public class ShoppingBasketTests
    {

        [TestMethod()]
        public void Adding_Three_Items_To_The_Basket_Works()
        {
            ShoppingBasket shoppingBasket = new ShoppingBasket();

            shoppingBasket.AddToBasket(new Product(0, "Bread", 1.00));
            shoppingBasket.AddToBasket(new Product(0, "Butter", 0.80));
            shoppingBasket.AddToBasket(new Product(0, "Milk", 1.15));
            Assert.AreEqual(shoppingBasket.Items.Keys.Count, 3);
        }

        [TestMethod()]
        public void One_Milk_One_Bread_One_Butter_Totals_2_95()
        {
            ShoppingBasket shoppingBasket = new ShoppingBasket();

            shoppingBasket.AddToBasket(new Product(0, "Bread", 1.00));
            shoppingBasket.AddToBasket(new Product(0, "Butter", 0.80));
            shoppingBasket.AddToBasket(new Product(0, "Milk", 1.15));

            Decimal basketTotal = new Decimal(shoppingBasket.GetBasketTotal());
            Decimal expectedResult = new Decimal(2.95);

            Assert.AreEqual(basketTotal, expectedResult);
        }

        [TestMethod()]
        public void Two_Butters_Two_Breads_Totals_3_10()
        {
            ShoppingBasket shoppingBasket = new ShoppingBasket();

            shoppingBasket.AddToBasket(new Product(0, "Bread", 1.00));
            shoppingBasket.AddToBasket(new Product(0, "Bread", 1.00));
            shoppingBasket.AddToBasket(new Product(0, "Butter", 0.80));
            shoppingBasket.AddToBasket(new Product(0, "Butter", 0.80));

            Decimal basketTotal = new Decimal(shoppingBasket.GetBasketTotal());
            Decimal expectedResult = new Decimal(3.10);

            Assert.AreEqual(basketTotal, expectedResult);
        }

        [TestMethod()]
        public void Four_Milks_Totals_3_45()
        {
            ShoppingBasket shoppingBasket = new ShoppingBasket();

            Product milk = new Product(0, "Milk", 1.15);

            shoppingBasket.AddToBasket(milk);
            shoppingBasket.AddToBasket(milk);
            shoppingBasket.AddToBasket(milk);
            shoppingBasket.AddToBasket(milk);

            Decimal basketTotal = new Decimal(shoppingBasket.GetBasketTotal());
            Decimal expectedResult = new Decimal(3.45);

            Assert.AreEqual(basketTotal, expectedResult);
        }

        [TestMethod()]
        public void Two_Butters_One_Bread_Eight_Milk_Totals_9_00()
        {
            ShoppingBasket shoppingBasket = new ShoppingBasket();

            Product milk = new Product(0, "Milk", 1.15);

            shoppingBasket.AddToBasket(new Product(0, "Butter", 0.80));
            shoppingBasket.AddToBasket(new Product(0, "Butter", 0.80));
            shoppingBasket.AddToBasket(new Product(0, "Bread", 1.00));
            shoppingBasket.AddToBasket(milk);
            shoppingBasket.AddToBasket(milk);
            shoppingBasket.AddToBasket(milk);
            shoppingBasket.AddToBasket(milk);
            shoppingBasket.AddToBasket(milk);
            shoppingBasket.AddToBasket(milk);
            shoppingBasket.AddToBasket(milk);
            shoppingBasket.AddToBasket(milk);

            Decimal basketTotal = new Decimal(shoppingBasket.GetBasketTotal());
            Decimal expectedResult = new Decimal(9.00);
            Assert.AreEqual(basketTotal, expectedResult);
        }

        [TestMethod()]
        public void ShoppingBasket_Constructor_Works()
        {
            ShoppingBasket shoppingBasket = new ShoppingBasket();
            Assert.IsNotNull(shoppingBasket);
        }

        [TestMethod()]
        public void AddToBasket_Adds_Item_Into_Basket()
        {
            ShoppingBasket shoppingBasket = new ShoppingBasket();

            shoppingBasket.AddToBasket(new Product(0, "Milk", 1.15));

            Assert.AreEqual(shoppingBasket.Items.Keys.Count, 1);
        }

        [TestMethod()]
        public void RemoveFromBasket_Removes_From_Basket()
        {
            ShoppingBasket shoppingBasket = new ShoppingBasket();
            Product p = new Product(0, "Milk", 1.15);

            shoppingBasket.AddToBasket(p);
            shoppingBasket.RemoveFromBasket(p);

            Assert.AreEqual(shoppingBasket.Items.Keys.Count, 0);

        }

        [TestMethod()]
        public void Twenty_Bread_Equals_20()
        {
            ShoppingBasket shoppingBasket = new ShoppingBasket();
            Product p = new Product(0, "Bread", 1);

            shoppingBasket.Items.Add(p.Name, new BasketItem(p));
            shoppingBasket.Items[p.Name].SetAmount(20);

            Decimal basketTotal = new Decimal(shoppingBasket.GetBasketTotal());
            Decimal expectedResult = new Decimal(20);
            Assert.AreEqual(basketTotal, expectedResult);

        }
    }
}