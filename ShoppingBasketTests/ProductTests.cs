﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingBasket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingBasket.Tests
{
    [TestClass()]
    public class ProductTests
    {
        [TestMethod()]
        public void Product_Constructor_Works()
        {
            Product product = new Product(0, "Product", 0);
            Assert.IsNotNull(product);
        }

        [TestMethod()]
        public void ToStringTest()
        {
            Product product = new Product(0, "Product", 0);
            Assert.IsTrue(product.ToString() is string);
        }
    }
}