﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingBasket
{
    public class Product
    {
        public long Id
        { get; set; }

        public string Name
        { get; set; }

        public double Price
        { get; set; }

        public Product(long id, string name, double price)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
        }


        override public string ToString()
        {
            string productToString = "ID : "+ this.Id.ToString()
                                    + " Name : " + this.Name +
                                    " Price : "+ this.Price.ToString();
            return productToString;
        }
    }
}
