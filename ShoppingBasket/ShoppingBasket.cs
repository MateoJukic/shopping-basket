﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingBasket

{
    public class ShoppingBasket
    {
        public Dictionary<string, BasketItem> Items
        { get; }
        
        // Discounts are stored in a list from which one can easily add or remove discounts.
        public IList<IDiscount> Discounts
        { get;  }

        public ShoppingBasket()
        {
            this.Items = new Dictionary<string, BasketItem>();
            this.Discounts = new List<IDiscount>();

            this.Discounts.Add(new BuyTwoXGetYDiscount("Butter","Bread",0.5));
            this.Discounts.Add(new EveryFourthXDiscount("Milk",1));    
        }

        public void AddToBasket(Product product)
        {
            // Check if the newly bought item was already in the basket.
            if (!this.Items.TryGetValue(product.Name,out BasketItem item))
            {
                BasketItem newItem = new BasketItem(product);
                this.Items.Add(product.Name, newItem);
            }
            else
            {
                item.Add();
            }
        }

        public void RemoveFromBasket(Product product)
        {
            // Check if the item is in the basket.
            if (!this.Items.TryGetValue(product.Name, out BasketItem i))
            {
                return;
            }
            this.Items.Remove(product.Name);
        }

        private double GetDiscount()
        {
            double discountTotal = 0;
            // Trying to apply all of the discounts that are available
            // CalculateDiscount returns a discount amount if discount is applicable.
            foreach (IDiscount discount in this.Discounts)
            {
                discountTotal += discount.CalculateDiscount(this.Items);
            }


            return discountTotal;
        }
        /* This method calculates the total amount every time it's called
         * alongside the discount.
        */
        public double GetBasketTotal()
        {
            double discountTotal = this.GetDiscount();
            this.LogItems();
            this.LogDiscounts();
           
            double basketTotal = 0;
            foreach( var item in this.Items)
            {
                basketTotal += item.Value.Total;
            }

            this.LogBasketTotal(basketTotal, discountTotal);

            return (basketTotal - discountTotal);
        }

        private void LogBasketTotal(double basketTotal, double discountTotal)
        {
            Console.WriteLine("##############################################");
            Console.WriteLine("Basket total : " + basketTotal.ToString()+"$");
            Console.WriteLine("Discount total : " + discountTotal.ToString()+"$");
            Console.WriteLine("Basket total after discount : " + (basketTotal - discountTotal).ToString()+"$");
        }

        private void LogItems()
        {
            Console.WriteLine("Items in the basket :");
            Console.WriteLine("##############################################");
            foreach (var item in this.Items)
            {
                Console.WriteLine(item.Value.ToString());
            }
        }

        private void LogDiscounts()
        {
            Console.WriteLine("##############################################");
            foreach (IDiscount discount in this.Discounts)
            {
                if (discount.IsApplicable(this.Items))
                {
                    Console.WriteLine(discount.ToString() + " was applied.");
                }
            }
        }
 

    }
}
