﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingBasket
{
    public class BuyTwoXGetYDiscount : IDiscount
    {
        private readonly string _xItemName;
        private readonly string _yItemName;
        private readonly double _itemDiscount;

        public BuyTwoXGetYDiscount(string xItem, string yItem, double itemDiscount)
        {
            this._xItemName = xItem;
            this._yItemName = yItem;
            this._itemDiscount = itemDiscount;
        }
        public double CalculateDiscount(Dictionary<string, BasketItem> items)
        {
            // Check if the discount is applicable.
            if (!this.IsApplicable(items))
            {
                return 0;
            }

            int xItemAmount = items[this._xItemName].GetAmount();
            int yItemAmount = items[this._yItemName].GetAmount();

            int timesDiscountIsApplicable = xItemAmount / 2;

            double discountAmount = 0;

            double itemPrice = items[this._yItemName].Item.Price;
            for (int i = 0; i < timesDiscountIsApplicable; i++) 
            {
                //Check in case the customer is eligible for more discounts than he has items that the discounts can be applied to.
                if (i == yItemAmount)
                {
                    break;
                }
                discountAmount += itemPrice * this._itemDiscount;
                
            }

            return discountAmount;
        }

        public bool IsApplicable(Dictionary<string, BasketItem> items)
        {
            // Check if xItem is in the cart.
            if (!items.TryGetValue(this._xItemName, out BasketItem xItem))
            {
                return false;
            }
            // Check if there are at least 2 of them in the cart.
            if (xItem.GetAmount() < 2) 
            {
                return false;
            }
            // Check if yItem is in the cart.
            if (!items.TryGetValue(this._yItemName, out BasketItem yItem))
            {
                return false;
            }
            return true;
        }
        override public string ToString()
        {
            string discountToString = "Buy 2 "+this._xItemName +"s and get one "+this._yItemName+" " + (this._itemDiscount*100).ToString()+"% off discount";
            return discountToString;
        }
    }
}
