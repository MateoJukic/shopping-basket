﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingBasket
{
    public class BasketItem
    {
        public Product Item
        { get; set; }
        private int _amount;
        public double Total
        { get; set; }

        public BasketItem(Product item)
        {
            this.Item = item;
            this._amount = 1;
            this.Total = item.Price;
        }
        public int GetAmount()
        {
            return this._amount;
        }
        public void SetAmount(int amount)
        {
            this._amount = amount;
            this.Total = amount * this.Item.Price;
        }

        public void Add()
        {
            this._amount += 1;
            this.Total += this.Item.Price;
        }

        override public string ToString()
        {
            string itemToString = this.Item.ToString() + " Amount : " + this._amount.ToString()
                                    + " Item total :" + this.Total.ToString();
            return itemToString;
        }
    }
}
