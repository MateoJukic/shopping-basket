﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingBasket
{
    public class EveryFourthXDiscount : IDiscount
    {
        private readonly string _itemName;
        private readonly double _itemDiscount;

        public EveryFourthXDiscount(string itemName, double itemDiscount)
        {
            this._itemName = itemName;
            this._itemDiscount = itemDiscount;
        }
        public double CalculateDiscount(Dictionary<string, BasketItem> items)
        {
            // Check if the discount is applicable.
            if (!this.IsApplicable(items))
            {
                return 0;
            }

            int itemAmount = items[this._itemName].GetAmount();

            int timesDiscountIsApplicable = itemAmount / 4;

            double discountAmount = 0;

            double itemPrice = items[this._itemName].Item.Price;

            for (int i = 0; i < timesDiscountIsApplicable; i++)
            {
                discountAmount += itemPrice * this._itemDiscount;
            }
            return discountAmount;
        }

        public bool IsApplicable(Dictionary<string, BasketItem> items)
        {
            BasketItem item;
            // Check if item is in the basket.
            if (!items.TryGetValue(this._itemName, out item))
            {
                return false;
            }
            //Check if there are at least 4 of them in the basket.
            // There needs to be a fourth one so that the discount can be applied.

            if (item.GetAmount() < 4)
            {
                return false;
            }
            
            return true;
        }
        override public string ToString()
        {
            string discountToString = "Buy 3 " + this._itemName + "s and get the 4th " + this._itemName +" "+ (this._itemDiscount * 100).ToString() + "% off discount";
            return discountToString;
        }
    }
}
