﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingBasket
{
    public interface IDiscount
    {
        double CalculateDiscount(Dictionary <string, BasketItem> items);

        bool IsApplicable(Dictionary<string, BasketItem> items);
    }
}
